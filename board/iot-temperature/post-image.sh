#!/bin/sh

# Enable i2c for temperature sensor
if ! grep -qE '^dtparam=i2c_arm=on' "${BINARIES_DIR}/rpi-firmware/config.txt"; then
	cat << __EOF__ >> "${BINARIES_DIR}/rpi-firmware/config.txt"

# enable i2c
dtparam=i2c_arm=on
dtparam=i2c_arm_baudrate=100000
__EOF__
fi

