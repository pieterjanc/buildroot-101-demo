buildroot-101-demo
==================
This is a very simple demo for creating an internet connected temperature sensor using buildroot. It was created for a Raspberry Pi 1, but can easily modified to run on other versions.

This project is structured as an br2-external tree so it is more clear what files were added.

Requirements
------------
* Raspberry Pi
* STS21 temperature sensor
* [buildroot](https://buildroot.org/download.html)

Building
--------
From the buildroot directory, run
```bash
$ make BR2_EXTERNAL=/path/to/this/project iot_temperature_defconfig
$ make
```

Flashing
--------
From the buildroot directory, run
```bash
$ sudo dd if=output/images/sdcard.img of=/dev/mmcblk0 bs=1M
```
where /dev/mmcblk0 is your SD card.

